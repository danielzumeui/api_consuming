package com.apiconsuming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiconsumingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiconsumingApplication.class, args);
    }

}
