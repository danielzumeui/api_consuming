package com.apiconsuming.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class HomeController {


    private TransactionRepository transactionRepository;

     @Autowired
    public HomeController(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @GetMapping("/index")
    public String getHello(){

        return "test";
    }
    @PostMapping("/save")
    @ResponseBody
    public void saveTransaction(@RequestBody Transaction transaction){
        transactionRepository.save(transaction);
    }
}
