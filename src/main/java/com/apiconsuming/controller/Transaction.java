package com.apiconsuming.controller;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Transaction {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long transactionId;
        private String fromAccount;
        private String toAccount;
        private double amount;
}

interface TransactionRepository extends CrudRepository<Transaction, Long> {

}